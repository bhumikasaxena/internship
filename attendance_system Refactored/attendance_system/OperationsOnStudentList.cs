﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attendance_system
{
    public class AddInfo_Calculate
    {
        public Display_Date display;
        public List<StudentCredential> students;
        public AddInfo_Calculate()
        {
            students = new List<StudentCredential>();
            display = new Display_Date();
        }
       
        public void AddInformation()
        {
            int arr = 0;
            Console.WriteLine("Enter the total number of students");
            try
            {
                int results = int.Parse(Console.ReadLine());


                for (int index = 0; index < results; index++) //change the limit of initialization
                {
                    int rollNumber = index;
                    Console.WriteLine("enter the name of the student");
                    string name =Console.ReadLine();
                    int output = 0;
                    if (int.TryParse(name, out output))
                    {
                        Console.WriteLine("Enter the value in string format");
                        break;
                    }
                  
                  
                   
                    StudentCredential student = new StudentCredential
                    {
                        RollNumber = rollNumber,
                        Name = name,
                        Attendancestore = display.Display_Dates()
                    };
                    students.Add(student);

                }

            }
            catch (Exception exception1)
            {
                string Message = exception1.Message;
                Console.WriteLine("Enter the correct input" + Message);


            }
        }
            public void CalculateAttendance()
            {
            int count = 0;
            foreach (StudentCredential student in students)
            {
                count++;
                int total_days_present = student.Attendancestore.Sum();
                if (total_days_present == 5 || total_days_present < 5)
                {
                    float total_days = 5;
                    float result = ((total_days_present / total_days) * 100);
                    Console.WriteLine("Rollno" + count + ":" + "Attendance percentage:" + result);
                    if (result == 75 || result > 75)
                    {
                        Console.WriteLine("You are allowed to sit in examination!!");
                    }
                    else
                        Console.WriteLine("You are not allowed to sit in exams due to low attendance!! ");

                }
            }
            }


        }
       
        }
        
      
        
    

    
