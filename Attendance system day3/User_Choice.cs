﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Application
{
    public class Options :Attendance, IUser_Choice
    {
        /// <summary>
        /// class to select the user choice if user select 1 ATTENDANCE function will be invoked 
        /// or if user select 2 then the Result function will be invoked
        /// </summary>
        public void Choices_for_user()
        {   try
            {
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        try
                        {
                            Console.WriteLine("If you are a professor the enter A or if you are student then enter B");
                            String option = (Console.ReadLine());
                            switch (option)
                            {
                                case "A":
                                    Console.WriteLine("Enter the name of The Proffesor");
                                    string name = Console.ReadLine();
                                    Get_Attendance(name);
                                    break;

                                case "B":
                                    Console.WriteLine("enter the Roll number of your student");
                                    int Roll_Number = int.Parse(Console.ReadLine());
                                    Get_Attendance(Roll_Number);
                                    break;
                                
                            }
                          
                        }
                        catch(Exception e)
                        {
                            string message = e.Message;
                            Console.WriteLine("enter either A or B " +message);
                        }
                        break;
                    case 2:
                        String Name;
                        Console.WriteLine("enter the name of the student");
                        Name = Console.ReadLine();
                        Get_Result(Name);
                        break;

                    default:
                        if (choice > 2 || choice < 1)
                        {
                            Console.WriteLine("choice is invalid either enter 1 or 2");
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                Console.WriteLine("Enter the correct value" + message);
                Console.WriteLine("enter the choice 1 for knowing  attendance\nenter the choice 2 for knowing the result of your child");
                Choices_for_user();
            }
        }
    }
}
