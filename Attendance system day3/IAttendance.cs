﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Application
{
    interface IAttendance
    {
        void Update_Attendance(float c);
        void Display_Attendance(float i);
        void Get_Attendance(int i);
        void Get_Attendance(String name);
    }
}
