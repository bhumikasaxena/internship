﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Application
{

    public class Information : Student,IInformation
    {/// <summary>
    /// this class contains the information of students 
    /// this class inherits student class and Interface IInformation
    /// </summary>
        public List<Student> studentList = new List<Student>()
                {
                new Student(){ StudentRollno=1, StudentName="Bharat",Securitycode=1234},
                new Student(){ StudentRollno=2, StudentName="Sanjay",Securitycode=5678},
                new Student(){ StudentRollno=3, StudentName="Ram",Securitycode=9012},
                new Student() { StudentRollno = 4, StudentName = "Moin",Securitycode = 3456}
                };
       public  Student s = new Student();

        public void Display()
        {
            foreach (Student students in studentList)
            {
                Console.WriteLine("Roll number : " + students.StudentRollno + " && " + "Name : " + students.StudentName);
            }
        }
    }
}

