﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmolyeeDBAccess.Utility;
using EmolyeeDBAccess;
namespace WebApplication1.Controllers
{
    public class EmployeeController : ApiController
    {
        EmployeeUtility employeeUtility = new EmployeeUtility();
        [HttpGet]
        public IHttpActionResult GetEmployeeInfo(int Id)
        {
            var result = employeeUtility.GetEmployee(Id);
            return Json(result);
        }
        [HttpDelete]
        public IHttpActionResult DeleteEmployee(int Id)
        {
            var result = employeeUtility.DeleteEmployee(Id);
            return Json(result);
        }
        [HttpPut]
        public IHttpActionResult PutEmployee(int Id, [FromBody] Employee emp)
        {
            var result = employeeUtility.AddEmployee(Id, emp);
            return Json(result);
        }
        [HttpPost]
        public IHttpActionResult PostEmployee(int id, [FromBody] Employee emp)
        {
            var result = employeeUtility.UpdateEmployee(id, emp);
            return Json(result);
        }

    }
}