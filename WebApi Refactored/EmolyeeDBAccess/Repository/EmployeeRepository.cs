﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmolyeeDBAccess.Repository
{
   public class EmployeeRepository
    {
        EmployeeDBEntities Context = new EmployeeDBEntities();
        public List<Employee> GetEmployees(int ID)
        {

            var result = Context.Employees.ToList();
            return result;

        }
        public  Employee GetEmployee(int Id)
        {
            var entity = Context.Employees.FirstOrDefault(e => e.ID == Id);          
            return entity;
        }
        public int DeleteEmployee(Employee emp)
        {
       
            Context.Employees.Remove(emp);
            int result = Context.SaveChanges();
            return result;
        }
        public string UpdateEmployee(int id,Employee emp)
        {
            var entity = Context.Employees.FirstOrDefault(e => e.ID == id);
            
                entity.FirstName = emp.FirstName;
                entity.LastName = emp.LastName;
                entity.Gender = emp.Gender;
                entity.Salary = emp.Salary;
            
           var result =  Context.SaveChanges();
            if (result == 0)
            {
                return "Employee not added";
            }
            else
                return "Employee added";
        }
        public int AddEmployee(Employee emp)
        {
            Context.Employees.Add(emp);
            int result = Context.SaveChanges();
            return result;
        }
    }
    }

