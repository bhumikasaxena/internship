﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmolyeeDBAccess.Repository;
using EmolyeeDBAccess;
namespace EmolyeeDBAccess.Utility
{
    public class EmployeeUtility
    {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        public List<Employee> GetEmployee(int Id)
        {
            var result = employeeRepository.GetEmployees(Id);
            return result;
        }
        public string DeleteEmployee(int Id)
        {
            var result = employeeRepository.GetEmployee(Id);
            if (result == null)
            {
                return "Employee does not exist";
            }
            else
            {
                int isSuccessful = employeeRepository.DeleteEmployee(result);
                if (isSuccessful == 0)
                {
                    return "Employee is not deleted";
                }
                else
                    return "Employee deleted!";
            }
        }
        public string UpdateEmployee(int id,Employee emp)
        {
             var result = employeeRepository.UpdateEmployee(id , emp);
            return result;
        }
        public string AddEmployee(int id, Employee employee)
        {
           

            var employeee = GetEmployee(id);
            bool isAlreadyExists = false;
            int result = -1;
            foreach (var employees in employeee)
            {
                if (employees.FirstName.ToLower() == (employee.FirstName + " " + employee.LastName).ToLower())
                {
                    isAlreadyExists = true;
                    break;
                }
            }
            if (!isAlreadyExists)
            {
                result = employeeRepository.AddEmployee(new Employee
                {
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    Gender = employee.Gender,
                    Salary = employee.Salary
                });
            }
                if (result > 0)
                    return "Employee added successfully.";
                else
                    return "Employee not added. Please check.";
            
        }
    }
}

