﻿using System;

namespace Sorting
{
    public class Class1
    {

        // sorting method to sort an array because it is neccessary condition for binary search that array should be sorted
        //sorting method we are using here is insertion sort
        public void sort(int[] variable_storage, int temporary_variabe, int temprory_storage, int number)
        {
            for (int index = 1; index <= number; index++)
            {
                temporary_variabe = variable_storage[index];
                temprory_storage = index - 1;
                while (temprory_storage >= 0 && temporary_variabe < variable_storage[temprory_storage])
                {
                    variable_storage[temprory_storage + 1] = variable_storage[temprory_storage];
                    temprory_storage--;
                }
                variable_storage[temprory_storage + 1] = temporary_variabe;
            }
            // Dispalys sorted array
            Console.WriteLine("array after sorting");
            for (int index = 1; index <= number; index++)
            {
                Console.WriteLine(variable_storage[index]);

            }
        }

    }
}