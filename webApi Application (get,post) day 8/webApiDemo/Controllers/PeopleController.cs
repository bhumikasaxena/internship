﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webApiDemo.Models;
namespace webApiDemo.Controllers
{
    

    public class PeopleController : ApiController
    {
        List<Person> people = new List<Person>();

        public PeopleController()
        {
            people.Add(new Person { FirstName = "BHUMIKA", LastName = "Saxena", Id = 1 });
            people.Add(new Person { FirstName = "Nikita", LastName = "Chaturvedi", Id = 2 });
            people.Add(new Person { FirstName = "Sakshi", LastName = "Malav", Id = 3 });

        }
        [Route ("api/People/GetFirstNames/{userId:int}/{age:int}")]
        [HttpGet]
        public List<String>GetFirstName(int userId,int age)
        {
            List<string> output = new List<string>();
            foreach (var index in people)
            {
                output.Add(index.FirstName);
            }
            return output;
        }

        // GET: api/People
        public List<Person> Get()
        {
            return people;
        }

        // GET: api/People/5
        public Person Get(int id)
        {
            return people.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST: api/People
        public void Post(Person val)
        {
            people.Add(val);
        }

        // DELETE: api/People/5
        public void Delete(int id)
        {
        }
    }
}
