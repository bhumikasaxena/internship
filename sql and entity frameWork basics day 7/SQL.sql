
Create table departments
(
	ID int primary key identity ,
	Name nvarchar(50),
	Location nvarchar(50)
)
Create table Employee
(
	ID int primary key identity ,
	FirstName nvarchar(50),
	LastName nvarchar(50),
	Gender nvarchar(50),
	Salary int,
	DepartmentId int foreign key references departments(ID)
)
Insert into departments values('IT','Newyork')
Insert into departments values('HR','London')
Insert into departments values('Payroll','Sydney')

Insert into Employee values('rajesh','jain','male','6000',1)
Insert into Employee values('mukes','sain','male','5000',3)
Insert into Employee values('rahul','sharma','male','4000',1)
Insert into Employee values('sushma','saxena','female','7000',2)
Insert into Employee values('kunjika','roy','female','8000',2)
Insert into Employee values('tanaya','deb','female','9000',3)
Insert into Employee values('mrigaya','gupta','female','1000',1)
select * from departments
select * from Employee;